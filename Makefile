# Converts CUDA to ROCm source via hipify.

.SUFFIXES:

cuda   = ${wildcard cuda/*.cu}
cuda_h = ${wildcard cuda/*.hh}
hip    = ${subst cuda,rocm,${addsuffix .hip.cc, ${basename ${cuda}}}}
hip_h  = ${subst cuda,rocm,${addsuffix .hip.hh, ${basename ${cuda_h}}}}

convert: ${hip} ${hip_h}

rocm/%.hip.cc: cuda/%.cu
	hipify-perl $< > $@

rocm/%.hip.hh: cuda/%.hh
	hipify-perl $< > $@

echo:
	@echo "cuda   ${cuda}"
	@echo "cuda_h ${cuda_h}"
	@echo "hip    ${hip}"
	@echo "hip_h  ${hip_h}"
