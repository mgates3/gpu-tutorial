#include "hip/hip_runtime.h"
/// ex00-empty.cu
/// Demonstrates compiling and launching an empty CUDA kernel.

#include <hip/hip_runtime.h>
#include <stdio.h>

#include "util.hh"

//------------------------------------------------------------------------------
/// GPU Kernel: executed by multiple threads on GPU.
///
__global__
void kernel()
{
}

//------------------------------------------------------------------------------
/// CPU Driver: executed on CPU, launches kernel on GPU.
///
void driver( hipStream_t stream )
{
    dim3 threads( 8, 16 );
    dim3 blocks( 10, 20, 30 );

    printf( "%% kernel<<< %d x %d x %d blocks, %d x %d x %d threads each >>>\n",
            blocks.x, blocks.y, blocks.z,
            threads.x, threads.y, threads.z );

    hipLaunchKernelGGL(kernel, dim3(blocks), dim3(threads), 0, stream );
    throw_error( hipGetLastError() );
}

//------------------------------------------------------------------------------
int main( int argc, char** argv )
{
    try {
        // Launch kernel, for now on null stream.
        driver( nullptr );
    }
    catch (std::exception const& ex) {
        fprintf( stderr, "Error: %s\n", ex.what() );
    }
    return 0;
}
