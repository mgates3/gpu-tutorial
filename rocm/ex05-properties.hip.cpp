#include <stdio.h>
#include <stdexcept>
#include <string>

#include "util.hh"

int main()
{
    try {
        int cnt;
        throw_error( hipGetDeviceCount( &cnt ) );
        printf( "device count %d\n", cnt );
        for (int dev = 0; dev < cnt; ++dev) {
            printf( "--------------------------------------------------------------------------------\n" );
            printf( "device                            %d\n", dev );

            hipDeviceProp_t prop;
            throw_error( hipGetDeviceProperties( &prop, dev ) );

            printf( "name                              %s\n",  // char[256]
                prop.name );
            //printf( "uuid                              ???\n" );
            printf( "totalGlobalMem                    %.2f GiB (%lu)\n", // size_t
                prop.totalGlobalMem / (1024.*1024*1024),
                prop.totalGlobalMem );
            printf( "sharedMemPerBlock                 %.0f KiB (%lu)\n", // size_t
                prop.sharedMemPerBlock / 1024.,
                prop.sharedMemPerBlock );
            printf( "regsPerBlock                      %d\n",  // int
                prop.regsPerBlock );
            printf( "warpSize                          %d\n",  // int
                prop.warpSize );
            printf( "memPitch                          %lu\n", // size_t
                prop.memPitch );
            printf( "maxThreadsPerBlock                %d\n",  // int
                prop.maxThreadsPerBlock );
            printf( "maxThreadsDim                     %d, %d, %d\n",  // int[3]
                prop.maxThreadsDim[0],
                prop.maxThreadsDim[1],
                prop.maxThreadsDim[2] );
            printf( "maxGridSize                       %d, %d, %d\n",  // int[3]
                prop.maxGridSize[0],
                prop.maxGridSize[1],
                prop.maxGridSize[2] );
            printf( "clockRate                         %.0f MHz (%d KHz)\n",  // int
                prop.clockRate / 1000.,
                prop.clockRate );
            printf( "totalConstMem                     %.0f KiB (%lu)\n", // size_t
                prop.totalConstMem / 1024.,
                prop.totalConstMem );
            printf( "major                             %d\n",  // int
                prop.major );
            printf( "minor                             %d\n",  // int
                prop.minor );
            printf( "textureAlignment                  %lu\n", // size_t
                prop.textureAlignment );
            printf( "texturePitchAlignment             %lu\n", // size_t
                prop.texturePitchAlignment );
            printf( "multiProcessorCount               %d\n",  // int
                prop.multiProcessorCount );
            printf( "kernelExecTimeoutEnabled          %d\n",  // int
                prop.kernelExecTimeoutEnabled );
            printf( "integrated                        %d\n",  // int
                prop.integrated );
            printf( "canMapHostMemory                  %d\n",  // int
                prop.canMapHostMemory );
            printf( "computeMode                       %d\n",  // int
                prop.computeMode );
            printf( "maxTexture1D                      %d\n",  // int
                prop.maxTexture1D );
            printf( "maxTexture1DLinear                %d\n",  // int
                prop.maxTexture1DLinear );
            printf( "maxTexture2D                      %d, %d\n", // int[2]
                prop.maxTexture2D[0],
                prop.maxTexture2D[1] );
            printf( "maxTexture3D                      %d, %d, %d\n", // int[3]
                prop.maxTexture3D[0],
                prop.maxTexture3D[1],
                prop.maxTexture3D[2] );
            printf( "concurrentKernels                 %d\n",  // int
                prop.concurrentKernels );
            printf( "ECCEnabled                        %d\n",  // int
                prop.ECCEnabled );
            printf( "pciBusID                          %d\n",  // int
                prop.pciBusID );
            printf( "pciDeviceID                       %d\n",  // int
                prop.pciDeviceID );
            printf( "pciDomainID                       %d\n",  // int
                prop.pciDomainID );
            printf( "tccDriver                         %d\n",  // int
                prop.tccDriver );
            printf( "memoryClockRate                   %.0f MHz (%d KHz)\n",  // int
                prop.memoryClockRate / 1000.,
                prop.memoryClockRate );
            printf( "memoryBusWidth                    %d\n",  // int
                prop.memoryBusWidth );
            printf( "l2CacheSize                       %.2f MiB (%d)\n",  // int
                prop.l2CacheSize / (1024.*1024),
                prop.l2CacheSize );
            printf( "maxThreadsPerMultiProcessor       %d\n",  // int
                prop.maxThreadsPerMultiProcessor );
            printf( "maxSharedMemoryPerMultiProcessor  %.0f KiB (%lu)\n", // size_t
                prop.maxSharedMemoryPerMultiProcessor / 1024.,
                prop.maxSharedMemoryPerMultiProcessor );
            printf( "managedMemory                     %d\n",  // int
                prop.managedMemory );
            printf( "isMultiGpuBoard                   %d\n",  // int
                prop.isMultiGpuBoard );
            printf( "pageableMemoryAccess              %d\n",  // int
                prop.pageableMemoryAccess );
            printf( "concurrentManagedAccess           %d\n",  // int
                prop.concurrentManagedAccess );
            printf( "cooperativeLaunch                 %d\n",  // int
                prop.cooperativeLaunch );
        }
    }
    catch (std::exception const& ex) {
        printf( "Error: %s\n", ex.what() );
    }
    return 0;
}
