/// ex01-matrix-norm.cu

#include <cuda_runtime.h>
#include <thrust/device_vector.h>
#include <vector>

#include "util.hh"

//==============================================================================
// Inf norm

const int matrix_norm_inf_nb = 64;

//------------------------------------------------------------------------------
/// GPU kernel: computes row sums:
///     sums[i] = sum_{j = 0...n} | Aij |.
///
/// Each thread-block computes one entry, sums[i].
/// Threads compute partial sums, then sum-reduce to get sums[i].
///
template <typename T>
__global__
void matrix_norm_inf_kernel_v1(
    int m, int n, T const* __restrict__ A, int ld, T* __restrict__ sums )
{
    const int nb = matrix_norm_inf_nb;
    assert( nb == blockDim.x );

    // Partial row sums, one per thread in thread block.
    __shared__ T s_partial_sums[ nb ];

    // Shift to i-th row.
    int i = blockIdx.x;
    T const* Ai = &A[ i ];

    // Thread tid sums:
    // s_partial_sums[ tid ] = A(i, tid) + A(i, tid + nb) + ... + A(i, tid + k*nb).
    int tid = threadIdx.x;
    s_partial_sums[ tid ] = 0;
    for (int j = tid; j < n; j += nb) {
        s_partial_sums[ tid ] += abs( Ai[ j*ld ] );
    }

    // Parallel binary tree sum reduction; result in s_partial_sums[ 0 ].
    int kb = nb / 2;
    while (kb > 0) {
        __syncthreads();
        if (tid < kb)
            s_partial_sums[ tid ] += s_partial_sums[ tid + kb ];
        kb /= 2;
    }

    // Save thread block's result.
    if (tid == 0) {
        sums[ i ] = s_partial_sums[ 0 ];
    }
}

//------------------------------------------------------------------------------
/// GPU kernel: computes row sums:
///     sums[i] = sum_{j = 0...n} | Aij |.
///
/// Each thread-block computes nb entries, sums[ k..k+nb ], k = blockIdx*nb.
/// Each thread sums one row.
///
template <typename T>
__global__
void matrix_norm_inf_kernel_v2(
    int m, int n, T const* __restrict__ A, int ld, T* __restrict__ sums )
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if (i < m) {
        // Shift to i-th row.
        T const* Ai = &A[ i ];

        // Thread sums across row i.
        T sum = 0;
        for (int j = 0; j < n; ++j) {
            sum += abs( Ai[ j*ld ] );
        }

        // Save thread's result.
        sums[ i ] = sum;
    }
}

//------------------------------------------------------------------------------
/// CPU driver computes inf norm of matrix, maximum row sum:
///     max_{i = 0...m} sum_{j = 0...n} | Aij |.
///
/// CPU driver launches kernels on GPU.
///
template <typename T>
T matrix_norm_inf(
    int m, int n, T const* A, int ld, cudaStream_t stream,
    int version, int verbose )
{
    // Workspace for m rows sums.
    thrust::device_vector<T> sums_vec( m );
    T* sums = thrust::raw_pointer_cast( sums_vec.data() );

    // Compute row sums.
    switch (version) {
        case 1: {
            // m blocks, nb threads each.
            int blocks = m;
            if (verbose) {
                printf( "m %d, n %d, matrix_norm_inf_kernel_v1<<< %d blocks, %d threads each >>>\n",
                        m, n, blocks, matrix_norm_inf_nb );
            }
            matrix_norm_inf_kernel_v1<<< blocks, matrix_norm_inf_nb, 0, stream >>>
                ( m, n, A, ld, sums );
            break;
        }

        case 2: {
            // ceil( m / nb ) blocks, nb threads each.
            int blocks = ceildiv( m, matrix_norm_inf_nb );
            if (verbose) {
                printf( "m %d, n %d, matrix_norm_inf_kernel_v2<<< %d blocks, %d threads each >>>\n",
                        m, n, blocks, matrix_norm_inf_nb );
            }
            matrix_norm_inf_kernel_v2<<< blocks, matrix_norm_inf_nb, 0, stream >>>
                ( m, n, A, ld, sums );
            break;
        }
        
        default:
            throw std::exception();
    }
    throw_error( cudaGetLastError() );

    // Get max row sum.
    T result = thrust::reduce(
        sums_vec.begin(), sums_vec.end(), 0, thrust::maximum<T>() );
    return result;
}

//==============================================================================
template <typename T>
void test( int m, int n, int align, int verbose )
{
    // Align columns, e.g., to 128-byte GPU cache line.
    int alignT = align / sizeof(T);
    alignT = std::max( alignT, 1 );
    int ld = ceildiv( m, alignT ) * alignT;

    // Allocate and initialize matrix on CPU host.
    std::vector<T> hA( ld * n );
    rand_matrix( m, n, hA.data(), ld );

    if (verbose >= 2) {
        print_matrix( "A", m, n, hA.data(), ld );
    }

    // Allocate matrix on GPU device and copy from host to device.
    thrust::device_vector<T> dA, dB, dC;
    dA = hA;

    // Get raw pointers to pass to CUDA kernel.
    T *dA_ptr;
    dA_ptr = thrust::raw_pointer_cast( dA.data() );

    // todo: Better error handling; this leaks GPU memory on error.
    cudaStream_t stream = nullptr;
    throw_error( cudaStreamCreate( &stream ) );

    for (int v = 1; v <= 2; ++v) {
        throw_error( cudaStreamSynchronize( stream ) );
        double time = get_wtime();

        //--------------------
        // Get matrix norm.
        T norm = matrix_norm_inf( m, n, dA_ptr, ld, stream, v, verbose );

        throw_error( cudaStreamSynchronize( stream ) );
        time = get_wtime() - time;

        // Print results. GB/s based on it reads A.
        double gbytes = m * n * sizeof(T) * 1e-9 / time;
        printf( "m %4d, n %4d, time %10.6f, GB/s %10.6f, norm = %.4e, version %d\n",
                m, n, time, gbytes, norm, v );
    }
    printf( "\n" );

    throw_error( cudaStreamDestroy( stream ) );
}

//------------------------------------------------------------------------------
int main( int argc, char** argv )
{
    int m = 8000;
    int n = 8000;
    int align = 4; // Minimal column alignment by default.
    int verbose = 0;
    int repeat = 5;

    // Rudimentary argument parsing.
    for (int i = 1; i < argc; ++i) {
        std::string arg( argv[i] );
        if (arg == "-m" && i+1 < argc) {
            m = strtol( argv[++i], nullptr, 0 );
        }
        else if (arg == "-n" && i+1 < argc) {
            n = strtol( argv[++i], nullptr, 0 );
        }
        else if (arg == "-align" && i+1 < argc) {
            align = strtol( argv[++i], nullptr, 0 );
        }
        else if (arg == "-v") {
            verbose += 1;
        }
        else if (arg == "-repeat" && i+1 < argc) {
            repeat = strtol( argv[++i], nullptr, 0 );
        }
        else {
            printf( "Unknown argument: %s\n", argv[i] );
        }
    }

    printf( "m %4d, n %4d, align %3d bytes\n",
            m, n, align );
    try {
        for (int i = 0; i < repeat; ++i) {
            test<float>( m, n, align, verbose );
        }
    }
    catch (std::exception const& ex) {
        fprintf( stderr, "Error: %s\n", ex.what() );
    }
    return 0;
}
