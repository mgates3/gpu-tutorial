/// ex01-matrix-add.cu

#include <cuda_runtime.h>
#include <vector>

#include "util.hh"

//------------------------------------------------------------------------------
/// GPU kernel: adds two m-by-n matrices: C = A + B.
/// Each thread adds one element, C(i,j) = A(i,j) + B(i,j).
///
__global__
void matrix_add_kernel(
    int m, int n, float const* A, float const* B, float* C )
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;

    if (i < m && j < n) {
        C[ i + j*m ] = A[ i + j*m ] + B[ i + j*m ];
    }
}

//------------------------------------------------------------------------------
/// CPU driver: adds two m-by-n matrices: C = A + B.
/// Launches kernel on GPU.
///
void matrix_add(
    int mb, int nb,
    int m, int n, float const* A, float const* B, float* C,
    cudaStream_t stream )
{
    dim3 threads( mb, nb );
    dim3 blocks( ceildiv( m, threads.x ),
                 ceildiv( n, threads.y ) );

    //printf( "%% m %d, n %d, matrix_add<<< %d x %d x %d blocks, %d x %d x %d threads each >>>\n",
    //        m, n,
    //        blocks.x, blocks.y, blocks.z,
    //        threads.x, threads.y, threads.z );

    matrix_add_kernel<<< blocks, threads, 0, stream >>>( m, n, A, B, C );
    throw_error( cudaGetLastError() );
}

//------------------------------------------------------------------------------
void test( int mb, int nb, int m, int n, bool verbose )
{
    // Allocate and initialize matrices on CPU host.
    std::vector<float> hA( m * n );
    std::vector<float> hB( m * n );
    std::vector<float> hC( m * n );
    rand_matrix( m, n, hA.data(), m );
    rand_matrix( m, n, hB.data(), m );
    rand_matrix( m, n, hC.data(), m );

    if (verbose) {
        print_matrix( "A", m, n, hA.data(), m );
        print_matrix( "B", m, n, hB.data(), m );
        print_matrix( "C", m, n, hC.data(), m );
    }

    // Allocate matrices on GPU device and copy from host to device.
    // todo: Better error handling; this leaks GPU memory on error.
    float *dA = nullptr, *dB = nullptr, *dC = nullptr;
    size_t size = m * n * sizeof(float);
    throw_error( cudaMalloc( &dA, size ) );
    throw_error( cudaMalloc( &dB, size ) );
    throw_error( cudaMalloc( &dC, size ) );

    throw_error( cudaMemcpy( dA, hA.data(), size, cudaMemcpyDefault ));
    throw_error( cudaMemcpy( dB, hB.data(), size, cudaMemcpyDefault ));
    throw_error( cudaMemcpy( dC, hC.data(), size, cudaMemcpyDefault ));

    cudaStream_t stream = nullptr;
    throw_error( cudaStreamCreate( &stream ) );
    throw_error( cudaStreamSynchronize( stream ) );
    double time = get_wtime();

    //--------------------
    // Add matrices.
    matrix_add( mb, nb, m, n, dA, dB, dC, stream );

    throw_error( cudaStreamSynchronize( stream ) );
    time = get_wtime() - time;
    // Reads A, B; writes C.
    double gbytes = 3 * size * 1e-9 / time;
    printf( "m %4d, n %4d, time %10.6f, GB/s %10.6f\n", m, n, time, gbytes );

    // Copy hC = dC (device to host).
    throw_error( cudaMemcpy( hC.data(), dC, size, cudaMemcpyDefault ));

    throw_error( cudaStreamDestroy( stream ) );
    throw_error( cudaFree( dA ) );
    throw_error( cudaFree( dB ) );
    throw_error( cudaFree( dC ) );

    if (verbose) {
        print_matrix( "Cout", m, n, hC.data(), m );
        printf( "%% In Matlab, check norm( A + B - Cout ) is ~ 1e-04,\n"
                "%% since matrices are printed to 4 decimal places.\n" );
    }
}

//------------------------------------------------------------------------------
int main( int argc, char** argv )
{
    int mb = 8;
    int nb = 8;
    int m = 20;
    int n = 13;
    int repeat = 5;
    int verbose = false;

    // Rudimentary argument parsing.
    for (int i = 1; i < argc; ++i) {
        std::string arg( argv[i] );
        if (arg == "-mb" && i+1 < argc) {
            mb = strtol( argv[++i], nullptr, 0 );
        }
        else if (arg == "-nb" && i+1 < argc) {
            nb = strtol( argv[++i], nullptr, 0 );
        }
        else if (arg == "-m" && i+1 < argc) {
            m = strtol( argv[++i], nullptr, 0 );
        }
        else if (arg == "-n" && i+1 < argc) {
            n = strtol( argv[++i], nullptr, 0 );
        }
        else if (arg == "-repeat" && i+1 < argc) {
            repeat = strtol( argv[++i], nullptr, 0 );
        }
        else if (arg == "-v") {
            verbose = true;
        }
        else {
            printf( "Unknown argument: %s\n", argv[i] );
        }
    }

    try {
        for (int i = 0; i < repeat; ++i) {
            test( mb, nb, m, n, verbose );
        }
    }
    catch (std::exception const& ex) {
        fprintf( stderr, "Error: %s\n", ex.what() );
    }
    return 0;
}
